public class Main {
    //Реализовать программу на Java, которая для последовательности чисел, оканчивающихся на -1
    // выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

    /**
     * функция принимает массив чисел, и разбивает их на одназначные числа
     * для поиска наименьшего значения
     * @param array
     * @return
     */
    public static int minimumSequenceNumber (int [] array) {
        int min = 9;
        for (int i = 0; i < array.length; i++) {
            if(array[i] != -1) {
                int num = array[i];
                int lastDigit;
                while (num != 0) {
                    lastDigit = num % 10;
                    if (min > lastDigit) {
                        min = lastDigit;
                    }
                    num = num / 10;
                }
            }else{
                return min;
            }
        }
        return min;
    }

    public static void main(String[] args) {
        int [] arrayNum = { 345, 555, 958, -1, -658, 345, 25};
        System.out.println(minimumSequenceNumber(arrayNum));
    }
}
