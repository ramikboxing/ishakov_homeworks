public class Main {
    //Задание 6.1
    //Реализовать функцию, принимающую на вход массив и целое число.
    // Данная функция должна вернуть индекс этого числа в массиве.
    // Если число в массиве отсутствует - вернуть -1.

    /**
     * Возвращает индекс числа в массиве
     *
     * @param array массив для анализа
     * @param num   число которое нужно найти в массиве
     * @return индек числа в массиве, если его нет то -1.
     */
    public static int getDigitID(int[] array, int num) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == num) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Процедура печати массива
     *
     * @param array
     */
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
    }

//Задание 6.2
//Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

    /**
     * Процедура сортировки массива обменом
     * сравниваються соседнии элементы массива,
     * если младший индек равен нулю а старший не равен,
     * то происходит обмен, счетчик итераций обнуляеться.
     * Цикл завершится когда счетчик итераций будет равен размеру массива,
     * т.е проход по массиву без перестановок.
     *
     * @param array
     */
    public static void exchangeSort(int[] array) {
        int i = 0;
        int goodPairsCount = 0; //  Количество итераций без перестановок
        while (true) {
            if ((array[i] == 0) && (array[i + 1] != 0)) {
                array[i] = array[i + 1];
                array[i + 1] = 0;
                goodPairsCount = 0; //Если были перестановки то счетчик обнулить
            } else {
                goodPairsCount++;  //иначе прибавить единицу
            }
            i++;
            if (i == array.length - 1) { // Если дошли до конца массива
                i = 0;                 //то начать сначала
            }
            if (goodPairsCount == array.length - 1) { // Если прошли по всему массиву без перестановок
                break; // то выход из цикла
            }
        } // while (true)
        printArray(array); // вывести массив на консоль
    }// exchangeSort(int [] array)

    /**
     * Процедура сортировки вставкой
     * Выполняется проход по массиву, как только находится нулевой элемент массива,
     * проход продолжается во вложенном цикле пока не найдет элемент больше нуля,
     * как только такой элемен находится происходит перестановка местами этих двух элементов
     * и вложенный цикл прерывается. Проход продолжается в основном цикле до конца массива
     *
     * @param array
     */
    public static void sortByInsertion(int array[]) {
        for (int i = 0; i < array.length; i++) { //Проход по массиву
            if (array[i] == 0) { // Если найден нулевой элемент
                for (int j = i; j < array.length; j++) { // то продолжаем проход по массиву
                    if (array[j] != 0) { // Пока не найдем не нулевой элемент
                        array[i] = array[j]; //и меняем их местами
                        array[j] = 0;
                        break; // и продолжаем первый проход по массиву
                    }
                }
            }
        }
        printArray(array); // вывести массив на консоль
    }

    public static void main(String[] args) {
        int[] a = {0, 3, 10, -5, 0, 23, 48, -30};
        //6.1
        int result = getDigitID(a, 23);
        if (result != -1) {
            System.out.println("the index of the number in the array " + result);
        } else {
            System.out.println("number in array not found");
        }
        //6.2
        printArray(a);
        System.out.println("");
        exchangeSort(a);
        sortByInsertion(a);
    }
}
