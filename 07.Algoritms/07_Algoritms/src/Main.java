import java.util.HashMap;
import java.util.Scanner;

/* На вход подается последовательность чисел, оканчивающихся на -1.
 Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
 Гарантируется: Все числа в диапазоне от -100 до 100.
                Числа встречаются не более 2 147 483 647-раз каждое.
                Сложность алгоритма - O(n)
*/
public class Main {
    static HashMap<Integer, Integer> myHashMap = new HashMap<Integer, Integer>();

    /**
     * Генератор случайных чисел от -100 до 100
     * (Math.random()*(100-(-100)))+(-100) => Math.random()*200-100
     *
     * @return
     */
    public static byte[] randomNumberGenerator() {
        byte[] array = new byte[1000000];
        for (int i = 0; i < array.length; i++) {
            array[i] = (byte) (Math.random() * 200 - 100);
        }
        return array;
    }

    public static int counterRepetitions(byte[] array) {
        int minCount = 2147483647; // Минимальное число повторений
        int number = 0;    //число из массива
        int count = 1; //Счетчик повторений , число встречается хотя бы раз
        for (int i = 0; i < array.length; i++) { // Запись элементов массива в хеш-таблицу
            number = array[i];                  // текущее число из массива
            if (number == -1) {                 // Поиск конца последовательности
                break;
            }
            if (number <= -100 && number >= 100) {  // Проверка дапазона числа
                System.out.println("Выход за пределы диапазона");
                break;
            }
            if (myHashMap.containsKey(number)) {
                myHashMap.put(number, myHashMap.get(number) + 1);
            } else {
                myHashMap.put(number, count);
            }
        } // for HashMap
        minCount = myHashMap.values().stream().min(Integer::compare).get(); // Поиск минимального значения в myHashMap
        return minCount;
    }

    public static void main(String[] args) {
        byte[] array = randomNumberGenerator();
        //byte[] array = {48, 54, 10, 48, 48, 2, 10, 56, 2, 2, 2, 10, 99, 54, 99, 10, 54, 10, 56, 56, 99};
        System.out.println("Minimum duplicates = " + counterRepetitions(array));
        System.out.println(myHashMap);
    }

}
