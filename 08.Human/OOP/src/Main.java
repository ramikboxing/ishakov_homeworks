public class Main {
    /**
     * Функция генерации людей
     *
     * @return
     */
    public static Human[] generatorPeople() { // Генератор людей
        String[] names = {"Rurik", "Oleg", "Igor", "Olga", "Svyatoslav", "Vladimer", "Yaroslav", "Yuri", "Ivan", "Vsevolod"};
        Human[] array = new Human[10];
        for (int i = 0; i < 10; i++) { // Создаем 10 экземпляров людей
            array[i] = new Human();
            array[i].setName(names[i]); // Присваеваем имена из списка
            array[i].setWeight((Math.random() * 50) + 50); // Случайным образом генерируем вес
        }
        return array;
    }

    /**
     * Процедура сортировки выбором
     *
     * @param people
     */
    public static void sortPeople(Human[] people) {
        int min, test;
        Human member = new Human();
        for (int i = 0; i < people.length; i++) {
            min = i;
            test = min;
            for (int j = i + 1; j < people.length; j++) {
                if (people[j].getWeight() < people[min].getWeight()) {
                    min = j;
                }
            }
            if (test != min) {
                member = people[min];
                people[min] = people[i];
                people[i] = member;
            }
        }
    }

    public static void main(String[] args) {

        Human[] people = generatorPeople();
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].getName() + " -  " + people[i].getWeight());
        }
        sortPeople(people);
        System.out.println("**********************************************");
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].getName() + " -  " + people[i].getWeight());
        }
    }
}
