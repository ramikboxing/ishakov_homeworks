public class Circle extends Ellipse {

    public Circle(double radius) {
        super(radius, radius);
    }

    public double getPerimeter() {
        return 2 * radius1 * 3.14;
    }
}
