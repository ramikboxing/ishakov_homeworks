public class Ellipse extends Figure {
    protected double radius1;
    private double radius2;

    public Ellipse(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    // Периметр Элипса: 4 * (((3,14 * a * b) + (a - b)^2)/(a + b)
    public double getPerimeter() {
        return 4 * (((3.14 * radius1 * radius2) + ((radius1 - radius2) * (radius1 - radius2))) / (radius1 + radius2));

    }

}
