// Сделать класс Figure, у данного класса есть два поля - x и y координаты.
// Классы Ellipse и Rectangle должны быть потомками класса Figure.
// Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
// В классе Figure предусмотреть метод getPerimeter(), который возвращает 0.
// Во всех остальных классах он должен возвращать корректное значение.

public class Main {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 15);
        rectangle.Figure(10, 5);
        rectangle.getCoordinates();
        System.out.println("Периметр прямоугольника " + rectangle.getPerimeter());

        Square square = new Square(5);
        square.Figure(5, 5);
        square.getCoordinates();
        System.out.println("Периметр квадрата " + square.getPerimeter());

        Ellipse ellipse = new Ellipse(10, 5);
        ellipse.Figure(0, 6);
        ellipse.getCoordinates();
        System.out.println("Периметр элипса "+ ellipse.getPerimeter());

        Circle circle = new Circle(8);
        circle.Figure(5, 9);
        circle.getCoordinates();
        System.out.println(" Периметр круга "+ circle.getPerimeter());

    }
}
