package game;

public interface PassiveSkill {
    int getBonus();
}
