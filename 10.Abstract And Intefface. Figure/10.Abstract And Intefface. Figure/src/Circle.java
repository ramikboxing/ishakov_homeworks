public class Circle extends Ellipse implements MoveFigure {

    public Circle(double radius) {
        super(radius, radius);
    }

    public double getPerimeter() {
        return 2 * radius1 * 3.14;
    }

    public void getCoordinates() {
        System.out.println("Circle: " + "x = " + this.x + " " + "y = " + this.y);
    }

    @Override
    public void moves(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
