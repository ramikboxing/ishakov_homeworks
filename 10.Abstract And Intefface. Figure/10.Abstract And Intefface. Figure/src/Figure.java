public abstract class Figure {
    //Координаты
    protected int x;
    protected int y;

    public void Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract void getCoordinates();

    public double getPerimeter() {
        return 0;
    }

}