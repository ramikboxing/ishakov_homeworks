// Сделать класс Figure из задания 09 абстрактным.
// Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
// Данный интерфейс должны реализовать только классы Circle и Square.
// В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.

public class Main {

    /**
     * Процедура перемещения "перемещаемых фигур"
     *
     * @param x     "координата перемещения x"
     * @param y     "координата перемещения y"
     * @param array "массив "перемещаемых фигур"
     */
    public static void MoveFigures(int x, int y, MoveFigure[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                array[i].moves(x, y);
            }
        }
    }

    public static void main(String[] args) {

        MoveFigure[] array = new MoveFigure[10]; //"массив "перемещаемых фигур"

        Square square = new Square(5);
        square.Figure(5, 5);
        array[0] = square; // Помещаем квадрат в массив "перемещаемых фигур"

        Square square1 = new Square(6);
        square1.Figure(6, 6);
        array[2] = square1; // Помещаем квадрат в массив "перемещаемых фигур"

        Square square2 = new Square(7);
        square2.Figure(7, 7);
        array[4] = square2; // Помещаем квадрат в массив "перемещаемых фигур"

        Circle circle = new Circle(8);
        circle.Figure(5, 9);
        array[1] = circle; // Помещаем круг в массив "перемещаемых фигур"

        Circle circle1 = new Circle(7);
        circle1.Figure(7, 5);
        array[3] = circle1; // Помещаем круг в массив "перемещаемых фигур"

        Circle circle2 = new Circle(10);
        circle2.Figure(6, 4);
        array[5] = circle2; // Помещаем круг в массив "перемещаемых фигур"

        System.out.println("Печать координат фигур");

        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                array[i].getCoordinates();
            }
        }

        MoveFigures(100, 50, array); //Вызов процедуры перемещения "перемещаемых фигур"

        System.out.println("Печать новых координат фигур");

        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                array[i].getCoordinates();

            }
        }

    }
}

