public class Rectangle extends Figure {
    protected int a;
    private int b;

    public Rectangle(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void getCoordinates() {
        System.out.println("Rectangle: " + "x = " + this.x + " " + "y = " + this.y);
    }

    public double getPerimeter() {
        return a * 2 + b * 2;
    }


}
