public class Square extends Rectangle implements MoveFigure {

    public Square(int a) {
        super(a, a);
    }

    public double getPerimeter() {
        return 4 * a;
    }

    public void getCoordinates() {
        System.out.println("Square: " + "x = " + x + " " + "y = " + y);
    }

    @Override
    public void moves(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
