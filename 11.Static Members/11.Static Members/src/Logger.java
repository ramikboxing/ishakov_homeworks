public class Logger {
    private static Logger logger;

    public static synchronized Logger getLogger() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }

    private Logger() {

    }

    public void log(String message) {
        System.out.println(message);
    }
}
