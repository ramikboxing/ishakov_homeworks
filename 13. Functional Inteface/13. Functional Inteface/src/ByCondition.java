@FunctionalInterface
public interface ByCondition {
    abstract boolean isOk(int number);
}
