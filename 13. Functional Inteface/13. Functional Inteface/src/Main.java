/*Предусмотреть функциональный интерфейс

interface ByCondition {
	boolean isOk(int number);
}
Реализовать в классе Sequence метод:

public static int[] filter(int[] array, ByCondition condition) {
	...
}
Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
В main в качестве condition подставить:
- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.
*/
public class Main {

    public static void main(String[] args) {
        int[] array = {122, 13, 24, 25, 56, 57};
        Sequence sequence = new Sequence();
        int[] arrayEvenNum;
        int[] arrayEvenSumDigits;
        arrayEvenNum = Sequence.filter(array, num -> num % 2 == 0); //Lambda реализация однострочная
        arrayEvenSumDigits = Sequence.filter(array, (num) -> {      //Lambda реализация расширенная
            int sum = 0;
            while (num != 0) {
                sum += num % 10;
                num /= 10;
            }
            return sum % 2 == 0;
        });
        // Реализация до Java 8
        /*  Sequence sequence = new Sequence();
            int[] arrayEvenNum;
            arrayFilter = Sequence.filter(array, new ByCondition() {
            @Override
            public boolean isOk ( int number){
                return (number % 2 == 0);
            }
        });*/
        for (int i = 0; i < arrayEvenNum.length; i++) {
            System.out.print(arrayEvenNum[i]+" ");

        }
        System.out.println("");

        for (int i = 0; i < arrayEvenSumDigits.length; i++) {
            System.out.print(arrayEvenSumDigits[i]+" ");

        }
    }


}
