public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int count = 0;
        int [] arrayCopy = new int [array.length];
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                arrayCopy[count] = array[i];
                count++;
            }
        }
        int[] filterArray = new int[count];
        for (int i = 0; i < count; i++) {
                filterArray[i] = arrayCopy[i];
        }
        return filterArray;
    }


}
