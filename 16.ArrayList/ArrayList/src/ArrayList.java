
public class ArrayList<T> {

    private static final int DEFAULT_SIZE = 10;
    private T[] elements;
    int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    /**
     * Добавляем элемент в конец списка
     *
     * @param element добавляемый элемент
     */
    public void add(T element) {
        if (isFullArray()) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {
        if (size == 0) {
            this.elements = (T[]) new Object[DEFAULT_SIZE];
        }
        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[size + size / 2];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получить элемент по индексу
     *
     * @param index индекс искомого элемента
     * @return элемент
     */
    public T get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.out.println("T get(int index) \n Индекс за пределами диапазона");
            return null;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Очистка списка, данные будут перезаписыыватся
     */
    public void clear() {
        this.size = 0;
    }

    /**
     * Удаление элемента по индексу
     *
     * @param index
     */
    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            if (size <= elements.length / 2) { // Если массив больше списка в два раза, то
                resize(); // Переделать размерность массива
            }
            for (int i = index; i < elements.length - 1; i++) { // Сместить все элементы в списке
                elements[i] = elements[i + 1];
            }
            size--; // Уменьшить размерность
        } else {
            System.out.println("index за пределами допустимых");
        }
    }

}
