public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> head; //Указатель на первый узел
    private Node<T> tail; //Указатель на последний узел;
    private int size;

    public void addBack(T element) { // Добавления узла в конец
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            head = newNode;
        } else {
            tail.next = newNode;
        }
        tail = newNode;
        size++;
    }

    public void addToBegin(T element) { // Добавления узла в начало
        Node<T> newNode = new Node<>(element);
        if (size == 0) {
            tail = newNode;
        } else {
            newNode.next = head;
        }
        head = newNode;
        size++;
    }

    public T get(int index) { //Вернуть значение по индексу
        if (index < 0 || index >= size) {
            System.out.println("Индекс за пределами размера списка");
            return null;
        }
        Node<T> getNode;
        getNode = head;     // Начнем искать с головы списка
        for (int i = 0; i <= index; i++) {
            if (i == index) {
                return getNode.value;
            }
            getNode = getNode.next;
        }
        return (T) getNode;
    }

    public int size() {
        return size;
    }

} // LinkedList<t>

