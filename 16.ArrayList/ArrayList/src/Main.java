import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // ArrayList
        ArrayList<String> lines = new ArrayList<>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        for (int i = 0; i < 25; i++) { // Добавить 25 случайных чисел
            numbers.add((int) (Math.random() * 99) + 1);
        }
        for (int i = 0; i < numbers.size; i++) { // Напечатать числа из списка
            System.out.print(numbers.get(i) + ",");
        }
        System.out.println("Размер массива = " + numbers.size); // Напечатать размер списка
        System.out.println("");

        for (int i = 0; i < 15; i++) { //Удалить последнии 15 чисел из списка
            numbers.removeAt(10);
        }

        for (int i = 0; i < numbers.size; i++) { // Напечатать числа из списка
            System.out.print(numbers.get(i) + ",");
        }
        System.out.println("Размер массива = " + numbers.size); // Напечатать размер списка

        //LinkedList
        System.out.println("LinkedList");
        LinkedList<Integer> list = new LinkedList<>();
        list.addBack(37);
        list.addBack(120);
        list.addBack(-10);
        list.addBack(11);
        list.addBack(50);
        list.addBack(-100);
        list.addBack(99);
        System.out.println(list.get(2));




    }
}
