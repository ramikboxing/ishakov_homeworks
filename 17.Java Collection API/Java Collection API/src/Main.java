/*На вход подается строка с текстом.
Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
Вывести:
Слово - количество раз
Использовать Map, string.split(" ") - для деления текста по словам.
Слово - символы, ограниченные пробелами справа и слева.
*/

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите текст");
        String str = scanner.nextLine(); // Ввести строку в str
        //System.out.println(str);

        Map<String, Integer> map = new HashMap<>();
        for (String strs : str.split(" ")) {

            if (map.containsKey(strs)) { // Если слово встречается в Map
                int x = map.get(strs); // Получить значение
                x++; // Увеличить
                map.put(strs, x); // Записать повторно с новым значением
                continue; // Завершить итерацию
            }
            if (strs == " ") { // Если строка пустая
                continue; // Завершить итерацию
            }
            map.put(strs, 1);
        }

        Set<Map.Entry<String, Integer>> mapStr = map.entrySet();
        for (Map.Entry<String, Integer> entry : mapStr) {
            if (entry == null) {
                continue;
            }
            System.out.println(entry.getKey() + "  - " + entry.getValue());
        }

    }
}
