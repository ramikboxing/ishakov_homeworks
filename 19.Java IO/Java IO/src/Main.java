import java.util.List;

public class Main {

    public static void main(String[] args) {
        String file = "user.txt"; // Файл с данными

        //     Выборка персон которым 35 лет
        UsersRepository usersRepository = new UsersRepositoryFileImpl(file); //Здесь читаем
        UsersRepository saveUser = new UsersRepositoryFileImpl("findByAge.txt"); //Сюда пишем

        List<User> usersAge = usersRepository.findByAge(35); // Список тех кому 35 лет

        for (User userAge : usersAge) {
            saveUser.save(userAge); // запишем в файл "findByAge.txt" тех кому 35
            System.out.println(userAge.getName() + " " + userAge.getAge());  // Печать тех кому 35
        }

        saveUser = new UsersRepositoryFileImpl("isWorkerTrue.txt"); //Сюда пишем

        List<User> usersWorker = usersRepository.findByIsWorkerIsTrue();

        for (User userWorker : usersWorker) {
            saveUser.save(userWorker); // запишем в файл "isWorkerTrue.txt" тех кто работает
            System.out.println(userWorker.getName() + " " + userWorker.getAge());
        }

        //        User user = new User("Igor", 33, true);
        //        usersRepository.save(user);

    }
}
