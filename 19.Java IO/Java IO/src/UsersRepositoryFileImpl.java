import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> listUsers = new ArrayList<>();
        Reader reader = null; //объявили переменные для доступа
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName); //создали читалку на основе файла
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine(); // прочитали строку
            while (line != null) {  // пока не пришла нулевая строка
                String[] parts = line.split("\\|"); // разбиваем ее по |

                if (parts[0].equals("")) { // Если попадеться пустая строка
                    line = bufferedReader.readLine();// считываем новую строку
                    continue; // переход к следующей итерации
                }

                String name = parts[0]; // берем имя
                int age = Integer.parseInt(parts[1]); //берем возраст
                boolean isWorker = Boolean.parseBoolean(parts[2]); //берем статус о работе

                User newUser = new User(name, age, isWorker); //создаем новый обьект User
                listUsers.add(newUser); // Добавить пользователя в список

                line = bufferedReader.readLine();// считываем новую строку
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close(); // Закрываем ресурсы
                } catch (IOException ignore) {
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close(); // Закрываем ресурсы
                    } catch (IOException ignore) {
                    }
                }
            }
        }
        return listUsers;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    /*Вытащит всех людей определенного возраста */
    @Override
    public List<User> findByAge(int ageUser) {
        //TODO: реализовать
        List<User> listFindAll = findAll();
        List<User> listUsersByAge = new ArrayList<>();
        for (User user : listFindAll) {
            if (user.getAge() == ageUser) {
                listUsersByAge.add(user);
            }
        }
        return listUsersByAge;
    }

    /*Вытащить всех кто работает */
    @Override
    public List<User> findByIsWorkerIsTrue() {
        //TODO: реализовать
        List<User> listFindAll = findAll();
        List<User> listIsWorker = new LinkedList<>();
        for (User user : listFindAll) {
            if (user.isWorker()) {
                listIsWorker.add(user);
            }
        }
        return listIsWorker;
    }
    
}
