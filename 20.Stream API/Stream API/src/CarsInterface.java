import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public interface CarsInterface {
    List<Car> findAll();

    List<String> carPlatesAreBlackOrZeroMileage();

    public long TheModelsInThePriceFrom700to800();

    public Optional<String> cheapestCarColor();

    public OptionalDouble CamryAverageCost();

}
