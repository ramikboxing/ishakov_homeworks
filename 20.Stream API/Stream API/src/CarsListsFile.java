import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;


public class CarsListsFile implements CarsInterface {

    private String fileName;

    public CarsListsFile(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAll() {
        List<Car> carList = new LinkedList();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName); //создали читалку на основе файла
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine(); // прочитали строку
            while (line != null) {  // пока не пришла нулевая строка
                String[] parts = line.split("\\|"); // разбиваем ее по |

                if (parts[0].equals("")) { // Если попадеться пустая строка
                    line = bufferedReader.readLine();// считываем новую строку
                    continue; // переход к следующей итерации
                }
                String number = parts[0];
                String model = parts[1];
                String color = parts[2];
                int mileage = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);
                Car car = new Car(number, model, color, mileage, price);
                carList.add(car);
                line = bufferedReader.readLine();// считываем новую строку
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close(); // Закрываем ресурсы
                } catch (IOException ignore) {
                }
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close(); // Закрываем ресурсы
                    } catch (IOException ignore) {
                    }
                }
            }
        }
        return carList;
    }

    @Override
    public List<String> carPlatesAreBlackOrZeroMileage() {
        return findAll().stream()
                .filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                .map(Car::getNumber)
                .collect(Collectors.toList());
    }

    @Override
    public long TheModelsInThePriceFrom700to800() {
        return findAll().stream()
                .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                .map(car -> car.getModel())
                .distinct()
                .count();
    }

    public Optional <String> cheapestCarColor() {
       return findAll().stream()
                .min((s1, s2) -> Integer.compare(s1.getPrice(), s2.getPrice()))
                .map(car -> car.getColor());

    }
 public OptionalDouble CamryAverageCost(){
        return findAll().stream()
                .filter(car -> car.getModel().equals("Camry"))
                .map(car -> car.getPrice())
                .mapToDouble(Integer::intValue)
                .average();
 }
}


