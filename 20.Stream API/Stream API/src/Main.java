/* Подготовить файл с записями, имеющими следующую структуру:
        [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

        o001aa111|Camry|Black|133|82000
        o002aa111|Camry|Green|133|0
        o001aa111|Camry|Black|133|82000

        Используя Java Stream API, вывести:

        Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
        Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
        * Вывести цвет автомобиля с минимальной стоимостью. // min + map
        * Среднюю стоимость Camry *

        https://habr.com/ru/company/luxoft/blog/270383/

        Достаточно сделать один из вариантов.
***********************************************************************************************************************
        // Промежуточные операторы стримов
        .distinct()  // выводит только уникальные значения

        .map(Function mapper) даёт возможность создать функию с помощью которой мы будем изменять каждый элемент
            и пропускать его дальше.
            (Функциональный интерфейс Function<T,R> представляет функцию перехода от объекта типа T к объекту типа R)

        .flatMap(Function<T, Stream<R>> mapper) — как и в случае с map, служат для преобразования в примитивный стрим

        .forEach(line -> System.out.println (line))  // печать всех строк

        .filter(Predicate predicate) фильтрует стрим, пропуская только те элементы, что проходят по условию
            (Predicate встроенный функциональный интерфейс, добавленный в Java SE 8 в пакет java.util.function.
             Проверяет значение на “true” и “false”);
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public class Main {

    public Main() {
    }

    public static void main(String[] args) {

        String file = "ListCars.txt";
        CarsInterface carsRepository = new CarsListsFile(file);

        // Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
        System.out.println("1. Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map");

        for (String car : carsRepository.carPlatesAreBlackOrZeroMileage()) {
            System.out.println(car);
        }

        // Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
        System.out.print("\n2.Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. = ");
        System.out.println(carsRepository.TheModelsInThePriceFrom700to800());

        // * Вывести цвет автомобиля с минимальной стоимостью. // min + map
        System.out.println("\n3.* Вывести цвет автомобиля с минимальной стоимостью. // min + map");
        carsRepository.cheapestCarColor().ifPresent(System.out::println);
        //* Среднюю стоимость Camry *
        System.out.println("\n4.*  Среднюю стоимость Camry");
        System.out.print("Средняя цена Camry = ");
        carsRepository.CamryAverageCost().ifPresent(System.out::print);

        System.out.println();
        // !!! НЕ ДЛЯ ПРОВЕРКИ
        // Реализация без доп. класса. Прямое чтение. Как в вебинаре.
        // System.out.println("Задание I. Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map");
        try (BufferedReader reader = new BufferedReader(new FileReader("ListCars.txt"))) {
            reader.lines()
                    // Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
                    .filter(s -> s.split("\\|")[2].equals("Black") || s.split("\\|")[3].equals("0"))
                    .map(str -> str.split("\\|")[0])
                    .forEach(System.out::println); //.forEach(line -> System.out.println(line));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        System.out.println();
        // System.out.print("Задание II. Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter = ");
        try (BufferedReader reader = new BufferedReader(new FileReader("ListCars.txt"))) {
            // Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
            long cheapCars = reader.lines()
                    .filter(auto -> {
                        String[] paramAuto = auto.split("\\|");
                        return Integer.parseInt(paramAuto[4]) >= 700000
                                && Integer.parseInt(paramAuto[4]) <= 800000;
                    })
                    .map(car -> car.split("\\|")[1])
                    .distinct()
                    .count();
            System.out.println(cheapCars);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }//main

}
