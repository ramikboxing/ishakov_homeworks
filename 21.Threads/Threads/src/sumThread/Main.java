package sumThread;

import java.util.*;

public class Main {
    public static int array[];
    //public static int sums[];

    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ввести размер массива");
        int numbersCount = scanner.nextInt(); //Размер массива
        System.out.println("Ввести кол-во потоков");
        int treadsCount = scanner.nextInt(); //кол-во пооков для подсчета

        array = new int[numbersCount];
        //sums = new int[treadsCount];
        // Random
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        int realSum = 0;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }
        long finishTime = System.currentTimeMillis();
        System.out.println(realSum);
        System.out.println("Время вычисления в одном потоке " + (finishTime - startTime));

        //TODO: реализовать работу с потоками

        int sectionSize = array.length / treadsCount; // секция для одного потока
        int remainder = array.length % treadsCount;   // остаток, который распределим по последним потока по одному
        int from = 0; //Начало секции потока
        int to = sectionSize; // конец секции потока

        // Создадим потоки размером без учета остатка
        List<SumThread> threadList = new LinkedList<>();
        startTime = System.currentTimeMillis();
        for (int i = 0; i < treadsCount - remainder; i++) {
            SumThread sumThread = new SumThread(from, to);
            sumThread.start(); // запуск потока
            threadList.add(sumThread);
            from = to;
            to += sectionSize;
        }
        sectionSize++;
        to++;
        //Остаток распределим равномерно по потокам
        for (int i = remainder; (i > 0 && i != 0); i--) {
            SumThread sumThread = new SumThread(from, to);
            sumThread.start();
            threadList.add(sumThread);
            from = to;
            to += sectionSize;
        }
        finishTime = System.currentTimeMillis();
        Thread.sleep(10);
        int byThreadSum = 0; // сумма потоков
        for (SumThread thread : threadList) {
            byThreadSum += thread.getSum();
        }

        System.out.println("Сумма подсчета потоков " + byThreadSum);
        System.out.println("Время вычисления потоками " + (finishTime - startTime));
    }
}

