package sumThread;

public class SumThread extends Thread {
    private int from;
    private int to;
    private int sum = 0;


    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;

    }

    @Override
    public void run() {
        Main main = new Main();
        for (int i = from; i < to; i++) {
            sum += main.array[i];
            // System.out.println(from);
        }
    }

    public int getSum() {
        return sum;
    }
}
