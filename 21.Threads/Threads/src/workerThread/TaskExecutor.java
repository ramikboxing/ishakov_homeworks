package WorkerThread;

public interface TaskExecutor {
    void submit(Runnable task);
}
