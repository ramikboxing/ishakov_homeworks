package WorkerThread;


import java.util.Deque;
import java.util.LinkedList;

public class TaskExecutorWorkerThread implements TaskExecutor {
    //очередь задач
    private final Deque<Runnable> tasks;
    // Побочный поток который будет выполнять какую-то работу
    // будет брать задачу из очереди и выполняет ее внутри себя
    private  final WorkerThread workerThread;

    public TaskExecutorWorkerThread() {
        this.tasks = new LinkedList<>();
        this.workerThread = new WorkerThread();
        this.workerThread.start();
    }

    private class WorkerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    while (tasks.isEmpty()) {
                        try {
                            // Пока задач нет уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException();
                        }
                    }
                    // забираем задачу из очереди
                    Runnable task = tasks.poll();
                    task.run();
                }
            }
        }
    }

    @Override
    public void submit(Runnable task) {
        // Как положить задачу в очередь? Если мы туда что-то кладем, значит из нее нельзя забирать
        synchronized (tasks) {
            // добавили задачу в очередь
            tasks.add(task);
            // как только положили задачу в очередь - оповестили ожидающий поток
            tasks.notify();
        }

    }
}
