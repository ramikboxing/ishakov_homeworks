import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:mysql://localhost:3306/db_homework24",
                "root", "Root481632");
        ProductsRepository productsRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);

        // Product product = new Product("bol", 45.2, 78);
        // productsRepository.save(product);
        // Задание 1.
        // System.out.println(productsRepository.findAll());
        List<Product> productList = productsRepository.findAll();
        for (Product pr : productList) {
            System.out.println(pr.getDescription() + " price-" + pr.getPrice() + "/ Кол-во-" + pr.getNumber());
        }
        // Задание 2.
        System.out.println(productsRepository.findAllByPrice(15));
        //Задание 3.
        List<Product> orderList = productsRepository.findAllByOrdersCount(2);
        for (Product or : orderList) {
            System.out.println(or.getDescription() + " price-" + or.getPrice() + "/ Кол-во-" + or.getNumber());
        }
    }
}