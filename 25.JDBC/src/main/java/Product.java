import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    private String description;
    private double price;
    private int number;


}
