import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.*;

public class ProductRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT = "INSERT INTO product (description, price, number) VALUES (?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM product ORDER BY id";
    //language=SQL
    private static final String SQL_SELECT_PRICE = "SELECT * FROM product WHERE price = ?";
    //language=SQL
    private static final String SQL_SELECT_ORDER = "SELECT * FROM product WHERE " +
            "(SELECT COUNT(id_product) FROM orderproduct WHERE id_product = product.id ) = ?";



    private JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        String description = row.getString("description");
        double price = row.getDouble("price");
        int number = row.getInt("number");

        return new Product(description, price, number);
    };

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getNumber());
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_ORDER, productRowMapper, ordersCount);
    }

}
