import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    //* найти все товары по количеству заказов, в которых участвуют
    List<Product> findAllByOrdersCount(int ordersCount);

    public void save(Product product);

}
