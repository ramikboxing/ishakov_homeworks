create table product
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(30),
    price       DOUBLE,
    number      INT
);
create table customer
(
    id         INT auto_increment primary key,
    firstName  VARCHAR(30),
    secondName VARCHAR(30)
);
create table orderProduct
(
    id_product  INT,
    id_order    INT,
    data_order  DATE,
    num_product int
);
ALTER TABLE orderProduct
    ADD FOREIGN KEY (id_product) REFERENCES product (id);
ALTER TABLE orderProduct
    ADD FOREIGN KEY (id_order) REFERENCES customer (id);
-- ввод данных в таблицы
INSERT INTO product
VALUES (null, 'pen', 1.5, 100);
INSERT INTO product
VALUES (null, 'pencil', 0.5, 300);
INSERT INTO product
VALUES (null, 'notebook', 15, 200);

INSERT INTO customer VALUE (null, 'Ivan', 'Petrov');
INSERT INTO customer VALUE (null, 'Robert', 'Kavalsci');
INSERT INTO customer VALUE (null, 'Mike', 'Kavalsci');
INSERT INTO customer VALUE (null, 'Nike', 'Reebook');

DELETE
FROM product
WHERE id = 5;
INSERT INTO orderProduct
VALUES (2, 2, '10.11.2020', 50);
INSERT INTO orderProduct
VALUES (1, 2, '10.11.2020', 20);
INSERT INTO orderProduct
VALUES (3, 1, '15.12.2020', 20);
INSERT INTO orderProduct
VALUES (null, null, '11.09.2020', 20);


-- обновить информацию
UPDATE orderproduct
SET num_product = 35
WHERE id_product = 3;
-- получить все данные из таблицы product
SELECT *
FROM product;
-- получить только имена заказчиков
SELECT firstName
FROM customer;
-- получить название и цены с сортировкой по возрастанию
SELECT description, price
FROM product
ORDER BY price;
-- получить название и цены с сортировкой по убыванию
SELECT description, price
FROM product
ORDER BY price desc;
-- получить всех, кто дороже рубля
SELECT *
FROM product
WHERE price > 1;
-- сколько позиций дороже рубля
SELECT count(*)
FROM product
WHERE price > 1;
-- какие фамилии всречаются
SELECT DISTINCT (secondName)
FROM customer;
-- какие фамилии  и сколько раз встречаются
SELECT secondName, count(*)
FROM customer
GROUP BY secondName;
-- имя заказчика с id 2 и количество его заказов
SELECT firstName,
       (SELECT COUNT(*)
        FROM orderproduct
        WHERE id_order = 2)
           AS orderproducr_count
FROM customer
WHERE id = 2;
-- получить имена заказчиков и количество их заказов
SELECT firstName,
       secondName,
       (SELECT COUNT(*) FROM orderproduct WHERE id_order = customer.id)
           AS oprder_count
FROM customer;
-- получить заказчиков которые заказали ручки
SELECT firstName, secondName
FROM customer
WHERE customer.id IN (SELECT id FROM product WHERE description = 'Pen');
-- все заказчики и заказы у которых есть заказчик
SELECT *
FROM customer c
         LEFT JOIN orderproduct p ON p.id_order = c.id;
-- все заказы и заказчики у которых есть заказы
SELECT *
FROM customer c
         RIGHT JOIN orderproduct p ON p.id_order = c.id;
-- только заказчики у которых есть заказы и их заказы
SELECT *
FROM customer c
         INNER JOIN orderproduct p ON p.id_order = c.id;

SELECT description, num_product
FROM product p
         INNER JOIN orderproduct o ON p.id = o.id_product;

SELECT description,
       price,
       number,
       (SELECT COUNT(id_product) FROM orderproduct WHERE id_product = product.id )
           AS order_count
FROM product ;

SELECT * FROM product
    WHERE
    (SELECT COUNT(id_product) FROM orderproduct WHERE id_product = product.id ) = 1;

SELECT * FROM product;
