public class NumbersUtil {
    //18,12=6//9,12=3//64,48=16
    public int gcd(int a, int b) {
        if (a < 0 || b < 0) {
            throw new IllegalArgumentException();
        }
        if (a == 0 || a == b) {
            return b;
        }
        while (b != 0) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    }
}
//            return a == 0 ? b : gcd(b % a, a);

