import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {
    //то, что мы будем тестировать
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class TestGCD {


        @ParameterizedTest(name = "throws exception on {0} , {1}")
        @CsvSource(value = {"-18, 12", "9, -12", "64, -48"})
        public void negative_numbers_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"0, 16", "0, 22", "0, 256"})
        public void if_a_null_return_b(int a, int b) {
            assertTrue(b == numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"16, 16", "22, 22", "256, 256"})
        public void if_a_equals_b_return_b(int a, int b) {
            assertTrue(b == numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"23, 27", "29, 34", "37, 38", "43, 46", "49,53", "83, 86", "57, 58", " 59, 67",
                "68,69", " 73,76", "74, 97", "78,89", "83, 94", "89,87"})
        public void ugly_numbers(int a, int b) {
            assertTrue(numbersUtil.gcd(a, b) == 1);
        }

    }//TestGCD
}