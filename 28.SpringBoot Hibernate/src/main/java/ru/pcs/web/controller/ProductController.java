package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.services.ProductService;

import java.util.List;

@Controller  // помечаем классы контролеры, это классы которые обрабатывают http запросы
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/products") // "/products" - название запроса URL
    public String getProductsPage(Model model) {
        List<Product> productList = productService._getAllProducts();
        model.addAttribute("products", productList); //"products" - название атрибута
        return "products"; //"products" - название страницы (возвращаемой)
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId) {
        Product newProduct = productService.getProduct(productId);
        model.addAttribute("product", newProduct);
        return "position";

    }

    @PostMapping("/products") // Пост запрос на добавление в БД
    public String addProduct(ProductForm form) {
        productService.addProduct(form);
        return "redirect:/products";
        //return "redirect:/product_add.html"; // вернуть пользователя на туже страницу
    }

    //POST localhost/products/7/delete
    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId) {
        productService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(ProductForm form, @PathVariable("product-id") Integer parameter) {
        productService.updateProduct(form, parameter);
        return "redirect:/products";
    }
}
