//package ru.pcs.web.repositories;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//import ru.pcs.web.models.Product;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//@Component
//public class ProductRepositoryJdbcTemplateImpl implements ProductsRepository {
//
//    //language=SQL
//    private static final String SQL_INSERT = "INSERT INTO product (description, price, number) VALUES (?, ?, ?)";
//    //language=SQL
//    private static final String SQL_SELECT_ALL = "SELECT * FROM product ORDER BY id";
//    //language=SQL
//    private static final String SQL_SELECT_PRICE = "SELECT * FROM product WHERE price = ?";
//    //language=SQL
//    private static final String SQL_SELECT_ORDER = "SELECT * FROM product WHERE " +
//            "(SELECT COUNT(id_product) FROM orderproduct WHERE id_product = product.id ) = ?";
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID = "DELETE FROM product WHERE id = ?";
//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "SELECT * FROM product WHERE id = ?";
//    //language=SQL
//    private static final String SQL_UPDATE_BY_ID = "UPDATE product SET description = ?, price=?, number=? WHERE id=?";
//
//    private JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
//        int id = row.getInt("id");
//        String description = row.getString("description");
//        double price = row.getDouble("price");
//        int number = row.getInt("number");
//
//        return new Product(id, description, price, number);
//    };
//
//    @Override
//    public void save(Product product) {
//        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getNumber());
//        System.out.println("save Repository");
//    }
//
//    @Override
//    public void delete(Integer productId) {
//        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
//    }
//
//
//    @Override
//    public void update(Product product) {
//        System.out.println("updateRepository");
//        System.out.println(product.getDescription()+", "+product.getPrice()+","+  product.getNumber()+","+ product.getId());
//        jdbcTemplate.update(SQL_UPDATE_BY_ID, product.getDescription(), product.getPrice(), product.getNumber(), product.getId());
//    }
//
//    @Override
//    public Product findById(Integer productId) {
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productRowMapper, productId);
//    }
//
//    @Override
//    public List<Product> findAll() {
//        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
//    }
//
//    @Override
//    public List<Product> findAllByPrice(double price) {
//        return jdbcTemplate.query(SQL_SELECT_PRICE, productRowMapper, price);
//    }
//
//    @Override
//    public List<Product> findAllByOrdersCount(int ordersCount) {
//        return jdbcTemplate.query(SQL_SELECT_ORDER, productRowMapper, ordersCount);
//    }
//
//}
