package ru.pcs.web.services;

import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;

import java.util.List;

public interface ProductService {
    void addProduct(ProductForm form);

    List<Product> _getAllProducts();

    void deleteProduct(Integer productId);

    Product getProduct(Integer productId);

    void updateProduct(ProductForm form, Integer idProduct);
}
