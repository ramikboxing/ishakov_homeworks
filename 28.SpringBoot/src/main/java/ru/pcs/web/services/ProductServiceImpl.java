package ru.pcs.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.ProductForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductsRepository;

import java.util.List;

@Component
public class ProductServiceImpl implements ProductService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .number(form.getNumber())
                .build();
        productsRepository.save(product);  //внесение данных в БД
    }

    @Override
    public List<Product> _getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) {
        productsRepository.delete(productId);
    }

    @Override
    public Product getProduct(Integer productId) {
        return productsRepository.findById(productId);
    }

    @Override
    public void updateProduct(ProductForm form , Integer idProduct) {
        Product product = Product.builder()
                .id(idProduct)
                .description(form.getDescription())
                .price(form.getPrice())
                .number(form.getNumber())
                .build();
        productsRepository.update(product);
    }

}
