package ru.pcs.web.forms;

import lombok.Data;

@Data

public class ProductForm {
    private int id;
    private String description;
    private Double price;
    private Integer number;
}
