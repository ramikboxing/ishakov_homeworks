package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.web.forms.AirPlaneForm;
import ru.pcs.web.forms.PilotForm;
import ru.pcs.web.models.AirPlane;
import ru.pcs.web.models.Pilot;
import ru.pcs.web.services.AirService;


import java.util.List;

@Controller // помечаем классы-контроллеры, это классы, которые способны обрабатывать HTTP - запросы
public class AirController {

    private final AirService airService;

    @Autowired
    public AirController(AirService airService) {
        this.airService = airService;
    }

    // Вывестит список самолетов
    @GetMapping("/planes")
    public String getPlanesPage(Model model) {
        List<AirPlane> planeList = airService.getAllPlanes();
        model.addAttribute("planes", planeList);
        return "planes";
    }

    //Вывести самолет
    @GetMapping("/planes/{plane-id}")
    public String getPlanePage(Model model, @PathVariable("plane-id") Integer planeId) {
        AirPlane newPlane = airService.getPlane(planeId);
        model.addAttribute("plane", newPlane);
        return "plane";
    }

    // Добавить самолет в БД
    @PostMapping("/planes")
    public String addPlane(AirPlaneForm form) {
        airService.addAirPlane(form);
        return "redirect:/planes";
    }

    // Удаление самолета
    @PostMapping("/planes/{plane-id}/delete")
    public String deleteAirPlane(@PathVariable("plane-id") Integer planeId) {
        airService.deletePlane(planeId);
        return "redirect:/planes";
    }

    // Обновить данные о самолете
    @PostMapping("/plane/{plane-id}/update")
    public String updatePlane(AirPlane form, @PathVariable("plane-id") Integer planeId) {
        airService.updatePlane(form, planeId);
        return "redirect:/planes";
    }

    // Пилоты
    // Вывести список пилотов
    @GetMapping("/pilots")
    public String getPilotsPage(Model model) {
        List<Pilot> pilotsList = airService.getAllPilots();
        model.addAttribute("pilots", pilotsList);
        return "pilots";
    }

    // Добавить пилота в БД
    @PostMapping("/pilots")
    public String addPilot(PilotForm form, BindingResult result, RedirectAttributes forRedirectModel) {
       if(result.hasErrors()){
           forRedirectModel.addFlashAttribute("errors", "Не добавлены данные");
           System.out.println("Сработала ОШИБКА");
           return "redirect:/pilots";
       }
        airService.addPilot(form);
        return "redirect:/pilots";
    }

    //Вывести пилота
    @GetMapping("/pilots/{pilot-id}")
    public String getPilotPage(Model model, @PathVariable("pilot-id") Integer planeId) {
        Pilot pilot = airService.getPilot(planeId);
        model.addAttribute("pilot", pilot);
        return "pilot";
    }

    // Обновить данные о пилоте
    @PostMapping("/pilot/{pilot-id}/update")
    public String updatePilot(Pilot form, @PathVariable("pilot-id") Integer planeId) {
        airService.updatePilot(form, planeId);
        return "redirect:/pilots";
    }
    // Удаление пилота
    @PostMapping("/pilots/{pilot_id}/delete")
    public String deletePilot(@PathVariable("pilot_id") Integer pilotId) {
        airService.deletePilot(pilotId);
        return "redirect:/pilots";
    }

}