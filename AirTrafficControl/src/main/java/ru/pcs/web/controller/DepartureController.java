package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.DepartureForm;
import ru.pcs.web.models.*;
import ru.pcs.web.services.DepartureService;

import java.util.List;

@RequiredArgsConstructor // Генерирует конструкторы
@Controller
public class DepartureController {

    private final DepartureService departureService;

    // Вывестит список вылетов, и свободные самолеты и пилоты
    @GetMapping("/departures")
    public String getDeparturesPage(Model model) {
        List<Departure> departureList = departureService.getAllDepartures();
        List<Pilot> freePilotList = departureService.getPilotWithoutDeparture();
        List<AirPlane> freeAirPlane = departureService.getPlaneWithoutDeparture();
        model.addAttribute("departures", departureList);
        model.addAttribute("free_pilots", freePilotList);
        model.addAttribute("free_boards", freeAirPlane);
        return "departures";
    }

    // Добавить вылет в БД
    @PostMapping("/departures")
    public String addDeparture(DepartureForm form) {
        departureService.addDeparture(form);
        return "redirect:/departures";
    }

    // Удаление вылета
    @PostMapping("/departures/{departure-id}/delete")
    public String deleteDeparture(@PathVariable("departure-id") Integer departureId) {
        departureService.deleteDeparture(departureId);
        return "redirect:/departures";
    }

    // Регистрация
    // Вывести список регистрации
    @GetMapping("/registrations")
    public String getRegistrationsPage(Model model) {
        List<Registration> registrationList = departureService.getAllRegistrations();
        model.addAttribute("registrations", registrationList);
        return "registrations";
    }
     //Список вылетов для регистрации
    @GetMapping("/registration_user")
    public String getRegistrationUser( @AuthenticationPrincipal(expression ="id")Integer userId, Model model) {
        List<Departure> departureList = departureService.getAllDepartures();
        model.addAttribute("departures", departureList);
        model.addAttribute("user_reg_id",userId);
        return "registration_user";
    }
    // Удаление регистрации
    @PostMapping("/registration/{registration_idReg}/delete")
    public String deleteRegistration(@PathVariable("registration_idReg") Integer registrationId) {
        departureService.deleteRegistration(registrationId);
        return "redirect:/registrations";
    }


    // Регистрация на рейс
    @PostMapping("/registration_user/{user_reg_id}/{departure_idDeparture}/add")
    public String addRegistration(@PathVariable("user_reg_id") Integer userId, @PathVariable ("departure_idDeparture") Integer departureId ) {
        departureService.addRegistrationUser(userId,departureId);
        return "redirect:/registration_user";
    }

}
