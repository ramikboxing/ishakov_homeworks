package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor // Генерирует конструкторы
@Controller
@RequestMapping("/singIn") //Все методы класса будут обрабатывать запрос "/singIn"
public class SingInController {

    @GetMapping
    public String getSingInPage() {
        return "singIn";
    }
}
