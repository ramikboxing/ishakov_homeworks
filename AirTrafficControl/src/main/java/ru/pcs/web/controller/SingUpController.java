package ru.pcs.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pcs.web.forms.SingUpForm;
import ru.pcs.web.services.SingUpService;

@RequiredArgsConstructor // Генерирует конструкторы
@Controller
@RequestMapping("/singUp") //Все методы класса будут обрабатывать запрос "/singUp"
public class SingUpController {

    private final SingUpService singUpService;

    @GetMapping
    public String getSingUpPage() {
        return "singUp";
    }

    @PostMapping
    public String singUpUser(SingUpForm form) {
        singUpService.singUpUser(form);
        return "redirect:/singIn";
    }
}
