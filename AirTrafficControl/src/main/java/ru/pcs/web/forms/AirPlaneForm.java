package ru.pcs.web.forms;

import lombok.Data;

@Data

public class AirPlaneForm {
    private Integer idPlane;
    private String typePlane;
    private Integer numberOfSeats;
    private Double rangeOfFlight;
}
