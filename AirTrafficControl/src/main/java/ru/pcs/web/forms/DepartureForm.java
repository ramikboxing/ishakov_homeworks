package ru.pcs.web.forms;

import lombok.Data;

@Data
public class DepartureForm {
    private Integer idDeparture;
    private Integer idBoard;
    private Integer idPilot;
    private String dateDeparture;
    private String destinationCountry;
    private String destinationCity;
}
