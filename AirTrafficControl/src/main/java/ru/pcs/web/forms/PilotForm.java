package ru.pcs.web.forms;

import lombok.Data;

@Data

public class PilotForm {
    private Integer idPilot;
    private String namePilot;
    private String surnamePilot;
    private Integer countOfDeparture;
}
