package ru.pcs.web.forms;

import lombok.Data;

@Data
public class RegistrationForm {
    private Integer idUserReg;
    private Integer idDepartureReg;
    private Integer idReg;
}
