package ru.pcs.web.forms;

import lombok.Data;

@Data
public class SingUpForm {
    //  private Integer id;

    private String nameUser;
    private String surnameUser;
    private String email;
    private String password;
}
