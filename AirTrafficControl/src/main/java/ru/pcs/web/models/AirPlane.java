package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "airplane")
public class AirPlane {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPlane;

    private String typePlane;

    private Integer numberOfSeats;

    private Double rangeOfFlight;


    @OneToOne(mappedBy = "board")
    private Departure departure;


}
