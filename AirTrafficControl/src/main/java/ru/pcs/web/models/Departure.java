package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Departure {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDeparture;

    @OneToOne
    @JoinColumn(name = "id_board")
    private AirPlane board;

    @OneToOne
    @JoinColumn(name = "id_pilot")
    private Pilot pilot;

    private String dateDeparture;
    private String destinationCountry;
    private String destinationCity;


}
