package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Pilot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPilot;

    private String namePilot;

    private String surnamePilot;

    private Integer countOfDeparture;

    @OneToOne(mappedBy = "pilot")
    private Departure departure;

}
