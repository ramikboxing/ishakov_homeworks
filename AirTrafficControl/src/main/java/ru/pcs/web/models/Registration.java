package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idReg;

    @ManyToOne
    @JoinColumn(name = "idUserReg")
    private User userReg;

    @OneToOne
    @JoinColumn(name = "idDepartureReg")
    private Departure departureReg;

}
