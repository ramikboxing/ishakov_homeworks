package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "user") // привязка класса к таблице в БД
public class User {

   public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String nameUser;
    private String surnameUser;

    @Column(unique = true) // email должен быть уникальным
    private String email;

    private String password;

    @OneToMany(mappedBy = "departureReg")
    private List<Registration> registrations;

}
