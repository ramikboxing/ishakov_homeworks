package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.AirPlane;

import java.util.List;

public interface AirPlaneRepository extends JpaRepository<AirPlane, Integer> {
    List<AirPlane> findAllByDepartureIsNull();
}
