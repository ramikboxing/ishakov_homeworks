package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Departure;

public interface DepartureRepository extends JpaRepository<Departure, Integer> {
}
