package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Pilot;

import java.util.List;

public interface PilotRepository extends JpaRepository<Pilot, Integer> {
    List<Pilot> findAllByDepartureIsNull();
}
