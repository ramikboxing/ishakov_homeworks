package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Registration;

public interface RegistrationRepository extends JpaRepository<Registration, Integer> {
}
