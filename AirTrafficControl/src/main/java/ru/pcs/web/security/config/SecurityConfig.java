package ru.pcs.web.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.pcs.web.security.detais.UserDetailsServiceImpl;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsServiceImpl).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/registration_user").authenticated()
                .antMatchers("/planes/**").hasAuthority("ADMIN")
                .antMatchers("/departures").hasAuthority("ADMIN")
                .antMatchers("/planes/**").hasAuthority("ADMIN")
                .antMatchers("/pilots/**").hasAuthority("ADMIN")
                .antMatchers("/registrations/**").hasAuthority("ADMIN")
                .antMatchers("/singUp").permitAll()
                .and()
                .formLogin()
                .loginProcessingUrl("/singIn")
                .loginPage("/singIn")
                .defaultSuccessUrl("/registration_user") //если регистрация удалась
                .usernameParameter("email")
                .passwordParameter("password")
                .permitAll();
    }
}
