package ru.pcs.web.security.detais;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pcs.web.models.User;

import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {

    private final User user;

    public UserDetailsImpl(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = user.getRole().toString();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role);
        return Collections.singleton(authority);
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override // Аккаунт не просрочен
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override //Не заблокирован аккаунт
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override //Не просрочен пароль
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override // аккаунт активен
    public boolean isEnabled() {
        return true;
    }

    public Integer id() {
        return user.getId();
    }

}
