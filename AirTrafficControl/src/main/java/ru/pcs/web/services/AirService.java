package ru.pcs.web.services;

import ru.pcs.web.forms.AirPlaneForm;
import ru.pcs.web.forms.PilotForm;
import ru.pcs.web.models.AirPlane;
import ru.pcs.web.models.Pilot;

import java.util.List;

public interface AirService {
    // AirPlanes
    void addAirPlane(AirPlaneForm form);

    List<AirPlane> getAllPlanes();

    void deletePlane(Integer planeId);

    AirPlane getPlane(Integer planeId);

    void updatePlane(AirPlane form, Integer planeId);

    // Pilot
    List<Pilot> getAllPilots();

    void addPilot(PilotForm form);

    Pilot getPilot(Integer planeId);

    void updatePilot(Pilot form, Integer planeId);

    void deletePilot(Integer pilotId);
}
