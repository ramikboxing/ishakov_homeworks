package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.web.exceptions.UserNotFoundException;
import ru.pcs.web.forms.AirPlaneForm;
import ru.pcs.web.forms.PilotForm;
import ru.pcs.web.models.AirPlane;
import ru.pcs.web.models.Pilot;
import ru.pcs.web.repositories.AirPlaneRepository;
import ru.pcs.web.repositories.PilotRepository;

import java.util.List;

@RequiredArgsConstructor // Генерация конструктора
@Component
public class AirServiceImpl implements AirService {

    private final AirPlaneRepository airPlaneRepository;
    private final PilotRepository pilotRepository;

//    @Autowired // Конструктор
//    public AirServiceImpl(AirPlaneRepository airRepository, DepartureRepository departureRepository) {
//        this.airPlaneRepository = airRepository;
//        this.departureRepository = departureRepository;
//    }

    @Override // добавить самолет
    public void addAirPlane(AirPlaneForm form) {
        AirPlane airPlane = AirPlane.builder()
                .typePlane(form.getTypePlane())
                .numberOfSeats(form.getNumberOfSeats())
                .rangeOfFlight(form.getRangeOfFlight())
                .build();
        airPlaneRepository.save(airPlane);
    }

    @Override  // Вернуть все самолеты
    public List<AirPlane> getAllPlanes() {
        return airPlaneRepository.findAll();
    }


    @Override // Удалить самолет по id
    public void deletePlane(Integer planeId) {
        airPlaneRepository.deleteById(planeId);
    }

    @Override
    public AirPlane getPlane(Integer planeId) {
        return airPlaneRepository.getById(planeId);
    }

    // Обновление данных о самолете
    @Override
    public void updatePlane(AirPlane form, Integer planeId) {
        AirPlane plane = AirPlane.builder()
                .idPlane(planeId)
                .typePlane(form.getTypePlane())
                .numberOfSeats(form.getNumberOfSeats())
                .rangeOfFlight(form.getRangeOfFlight())
                .build();
        airPlaneRepository.save(plane);
    }

    // Вывести всех пилотов
    @Override
    public List<Pilot> getAllPilots() {
        return pilotRepository.findAll();
    }

    // Добавить пилота
    @Override
    public void addPilot(PilotForm form) {
        Pilot newPilot = Pilot.builder()
                .namePilot(form.getNamePilot())
                .surnamePilot(form.getSurnamePilot())
                .countOfDeparture(form.getCountOfDeparture())
                .build();
        pilotRepository.save(newPilot);
    }

    // Вывести пилота
    @Override
    public Pilot getPilot(Integer planeId) {
        return pilotRepository.findById(planeId).orElseThrow(UserNotFoundException::new);  //getById(planeId);
    }

    // Обновить данные о пилоте
    @Override
    public void updatePilot(Pilot form, Integer planeId) {
        Pilot pilot = Pilot.builder()
                .idPilot(planeId)
                .namePilot(form.getNamePilot())
                .surnamePilot(form.getSurnamePilot())
                .countOfDeparture(form.getCountOfDeparture())
                .build();
        pilotRepository.save(pilot);
    }

    //Удалить пилота
    @Override
    public void deletePilot(Integer pilotId) {
        pilotRepository.deleteById(pilotId);
    }


}
