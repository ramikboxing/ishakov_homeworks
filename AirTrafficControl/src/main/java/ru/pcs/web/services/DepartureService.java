package ru.pcs.web.services;

import ru.pcs.web.forms.DepartureForm;
import ru.pcs.web.models.AirPlane;
import ru.pcs.web.models.Departure;
import ru.pcs.web.models.Pilot;
import ru.pcs.web.models.Registration;

import java.util.List;

public interface DepartureService {
    //Departure
    List<Departure> getAllDepartures();

    void addDeparture(DepartureForm form);

    void deleteDeparture(Integer departureId);

    List<Pilot> getPilotWithoutDeparture();

    List<AirPlane> getPlaneWithoutDeparture();

    //Registration
    List<Registration> getAllRegistrations();

    void addRegistrationUser(Integer userId, Integer departureId);

    void deleteRegistration(Integer registrationId);
}
