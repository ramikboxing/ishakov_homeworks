package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.DepartureForm;
import ru.pcs.web.models.AirPlane;
import ru.pcs.web.models.Departure;
import ru.pcs.web.models.Pilot;
import ru.pcs.web.models.Registration;
import ru.pcs.web.repositories.*;

import java.util.List;

@RequiredArgsConstructor // Генерация конструктора
@Component
public class DepartureServiceImpl implements DepartureService {

    private final AirPlaneRepository airPlaneRepository;
    private final DepartureRepository departureRepository;
    private final PilotRepository pilotRepository;
    private final RegistrationRepository registrationRepository;
    private final UserRepository userRepository;

    // ВЫЛЕТ
    //Вернуть все вылеты
    @Override
    public List<Departure> getAllDepartures() {
        return departureRepository.findAll();
    }

    // Добавить вылет
    @Override
    public void addDeparture(DepartureForm form) {
        Departure newDeparture = Departure.builder()
                .board(airPlaneRepository.getById(form.getIdBoard()))
                .pilot(pilotRepository.getById(form.getIdPilot()))
                .dateDeparture(form.getDateDeparture())
                .destinationCountry(form.getDestinationCountry())
                .destinationCity(form.getDestinationCity())
                .build();
        departureRepository.save(newDeparture);
    }

    // Удалить вылет
    @Override
    public void deleteDeparture(Integer departureId) {
        departureRepository.deleteById(departureId);
    }

    //Вывести свободных пилотов
    @Override
    public List<Pilot> getPilotWithoutDeparture() {
        return pilotRepository.findAllByDepartureIsNull();
    }

    //Вывести свободные самолеты
    @Override
    public List<AirPlane> getPlaneWithoutDeparture() {
        return airPlaneRepository.findAllByDepartureIsNull();
    }

    // Вывести все регистрации
    @Override
    public List<Registration> getAllRegistrations() {
        return registrationRepository.findAll();
    }

    // Добавить регистратцию
    @Override
    public void addRegistrationUser(Integer userId, Integer departureId) {
        Registration newRegistration = Registration.builder()
                .userReg(userRepository.getById(userId))
                .departureReg(departureRepository.getById(departureId))
                .build();
        registrationRepository.save(newRegistration);
    }

    // Удалить регистрацию
    @Override
    public void deleteRegistration(Integer registrationId) {
        registrationRepository.deleteById(registrationId);
    }

}



