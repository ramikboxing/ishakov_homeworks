package ru.pcs.web.services;

import ru.pcs.web.forms.SingUpForm;

public interface SingUpService {
    void singUpUser(SingUpForm form);
}
