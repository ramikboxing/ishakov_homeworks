package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.SingUpForm;
import ru.pcs.web.models.User;
import ru.pcs.web.repositories.UserRepository;


@RequiredArgsConstructor
@Component
public class SingUpServiceImpl implements SingUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Override
    public void singUpUser(SingUpForm form) {
        User newUser = User.builder()
                .nameUser(form.getNameUser())
                .surnameUser(form.getSurnameUser())
                .email(form.getEmail())
                .role(User.Role.USER)
                .password(passwordEncoder.encode(form.getPassword()))
                .build();
        userRepository.save(newUser);
    }
}
