CREATE DATABASE db_AirTrafficControl;
USE db_airtrafficcontrol;

CREATE TABLE user(id INT AUTO_INCREMENT PRIMARY KEY,
    name_user VARCHAR (20), surname_user VARCHAR (20), email VARCHAR (40), password VARCHAR (40) );

SHOW TABLES;
DESC user;

CREATE TABLE airplane (id_plane INT AUTO_INCREMENT PRIMARY KEY,
    type_plane VARCHAR (15), number_of_seats INT, range_of_flight DOUBLE, available_for_departure BOOLEAN);

CREATE TABLE pilot (id_pilot INT AUTO_INCREMENT PRIMARY KEY,
     name_pilot VARCHAR(20), surname_pilot VARCHAR(20), count_of_departure INT, available_for_flight BOOLEAN);

CREATE TABLE departure (id_departure INT AUTO_INCREMENT PRIMARY KEY,
    id_board INT, id_pilot INT, date_departure DATETIME, destination_country VARCHAR (20), destination_city VARCHAR (20));
ALTER TABLE departure ADD FOREIGN KEY (id_board) REFERENCES airplane (id_plane);

SHOW CREATE TABLE departure;
      ALTER TABLE departure DROP FOREIGN KEY departure_ibfk_1;

ALTER TABLE departure ADD FOREIGN KEY (id_pilot) REFERENCES pilot (id_pilot);
      ALTER TABLE departure DROP FOREIGN KEY departure_ibfk_2;

CREATE TABLE registration (id_user_reg INT, id_departure_reg INT);
ALTER TABLE registration ADD FOREIGN KEY (id_user_reg) REFERENCES user (id);
ALTER TABLE registration ADD FOREIGN KEY (id_departure_reg) REFERENCES departure (id_departure);
ALTER TABLE registration ADD COLUMN  id_reg INT AUTO_INCREMENT PRIMARY KEY ;

SELECT * FROM airplane;
ALTER TABLE  airplane DROP COLUMN  id_plane;

DROP TABLE pilot;

ALTER TABLE  pilot DROP COLUMN available_for_flight;
ALTER  TABLE  user CHANGE COLUMN password password VARCHAR(100);