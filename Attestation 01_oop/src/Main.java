/*Реализовать в классе UsersRepositoryFileImpl методы:

User findById(int id);
update(User user);

Принцип работы методов: Пусть в файле есть запись:

1|Игорь|33|true

Где первое значение - гарантированно уникальный ID пользователя (целое число).
Тогда findById(1) вернет объект user с данными указанной строки.
Далее, для этого объекта можно выполнить следующий код:
user.setName("Марсель");
user.setAge(27);
и выполнить update(user);
При этом в файле строка будет заменена на 1|Марсель|27|true.
Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.
Примечания:
Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)
*/

import java.util.List;

public class Main {

    public static void main(String[] args) {
        String file = "UserList.txt"; // data file
        UserRepositoryInterface userRepositoryInterface = new UsersRepositoryFileImpl(file); //Здесь читаем
        List<User> users = userRepositoryInterface.findAll(); // list all users

        for (User user : users) {
            //    System.out.println(user.getName());// print  users from list
        }

        User userM = new User("Makar", 31, false); // create new user
        userRepositoryInterface.save(userM);  // save new user to data file

        User user = userRepositoryInterface.findById(3);
        user.setName("Olga2");
        user.setAge(21);
        user.setWorker(false);
        userRepositoryInterface.update(user);// update user id -3
    }
}
