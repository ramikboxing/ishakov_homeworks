import java.util.List;

public interface UserRepositoryInterface {
    List<User> findAll();
    void save(User user);
    User findById(int id);
    void update(User user);

}
