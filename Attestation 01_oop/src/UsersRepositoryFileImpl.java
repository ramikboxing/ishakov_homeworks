import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UserRepositoryInterface {
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Записать в список User из файла
     * @return userList - список
     */
    @Override
    public List<User> findAll() {
        List<User> userList;
        try (BufferedReader reader = new BufferedReader(new FileReader("UserList.txt"))) {
            userList = reader.lines()
                    .map(str -> str.split("\\|"))
                    .filter(str -> (str[0].equals("") == false))
                    .distinct()
                    .map(str -> new User(Integer.parseInt(str[0]),
                            str[1],
                            Integer.parseInt(str[2]),
                            Boolean.parseBoolean(str[3])))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return userList;
    }

    /**
     * Записать  пользователя в конец файла
     * numId - максимальный id в файле
     * @param user
     */
    @Override
    public void save(User user) {
        int numId = findAll().stream()
                .map(str -> str.getId())
                .distinct()
                .max(Integer::compare)
                .get();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("UserList.txt", true))) {
            writer.write(++numId + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Вернуть User по id в файле
     * @param id
     * @return User
     */
    @Override
    public User findById(int id) {
        User user = findAll().stream()
                .filter(person -> person.getId() == id)
                .findAny()
                .get();
        return user;
    }

    @Override
    public void update(User user) {
        List<User> userList = findAll();
        userList.stream()
                .filter(person -> person.getId() == user.getId())
                .peek(emp -> emp.setName(user.getName()))
                .peek(emp -> emp.setAge(user.getAge()))
                .peek(emp -> emp.setWorker(user.isWorker()))
                .collect(Collectors.toList());

            try (BufferedWriter writer = new BufferedWriter(new FileWriter("UserList.txt", false))) {
                for (User u : userList) {
                    writer.write(u.getId() + "|" + u.getName() + "|" + u.getAge() + "|" + u.isWorker());
                    writer.newLine();
                    System.out.println();
                }
                writer.flush();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }


