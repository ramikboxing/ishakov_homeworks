package ru.iskhakov.springcourse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component

public class ClassicalMusic implements Music {
    @PostConstruct
    public void doMyInit(){
        System.out.println("Doing my initialization");
    }
    @PreDestroy
    public void doMyDestroy(){
        System.out.println("Doing my destroy");
    }
    @Override
    public String getSong() {
        return " MocartMusic";
    }

    @Override
    public String getSong(int track) {
        String[] listStr = {"Bethowin Music", "Chaikovski", "Mocart"};
        return listStr[track];
    }

}
