package ru.iskhakov.springcourse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Computer {
    private int id;
    private MusicPlayer musicPlayer;

    @Autowired
    public Computer(MusicPlayer musicPlayer) {
        this.id = 1;
        this.musicPlayer = musicPlayer;
    }
    int track = 0+ (int)(Math.random()*3);

    @Override
    public String toString() {
        System.out.println(track);
        return "Computer" + id + " " + musicPlayer.playMusic(MusicPlayer.GenreMusic.ROCK, track);
    }
}
