package ru.iskhakov.springcourse;

public interface Music {
    String getSong();
    String getSong(int track);
}
