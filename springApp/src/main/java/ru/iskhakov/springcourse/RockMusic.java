package ru.iskhakov.springcourse;

import org.springframework.stereotype.Component;

@Component
public class RockMusic implements Music{
    @Override
    public String getSong() {
        return "Zwitter";
    }

    @Override
    public String getSong(int track) {
        String[] listStr = {"DuHast", "RammsteinRock", "SeaMan"};
        return listStr[track];
    }
}
