package ru.iskhakov.springcourse;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.iskhakov.springcourse")
@PropertySource("classpath:musicPlayer.properties")
public class SpringConfig {

}
