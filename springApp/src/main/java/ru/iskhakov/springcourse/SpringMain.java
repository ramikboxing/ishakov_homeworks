package ru.iskhakov.springcourse;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
     //   Computer computer = context.getBean("computer", Computer.class);
     //   System.out.println(computer);

      //  MusicPlayer musicPlayer = context.getBean("musicPlayer", MusicPlayer.class);
     //   System.out.println(musicPlayer.getName() + " " + musicPlayer.getVolume());
     //   ClassicalMusic classicalMusic = context.getBean("classicalMusic", ClassicalMusic.class);
      //  context.close();
    }
}
